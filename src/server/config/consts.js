module.exports = {
	//At which interval does each zone tick in ms
	tickTime: 350,

	//The maximum level a player can reach
	maxLevel: 20,

	//How far a player can see objects horizontally
	viewDistanceX: 25,

	//How far a player can see objects vertically
	viewDistanceY: 14
};
